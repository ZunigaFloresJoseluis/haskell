![haskell](https://iscpif.fr/wp-content/uploads/2018/01/haskell-logo.png)

# Haskell

Haskell es un lenguaje puramente funcional y compilado su nombre se le debe al logico estadounidense Haskell curry
debido a los aportes al calculo lambda

Caracteristicas
* Haskell es perezoso. A menos que  se vea forzado a ejecutar una función la ejecutara.
* Tipado Estaticamente
* Elegante y consiso


### Ciudadanos de primera Clase

Para Haskell  una función es un ciudadano de primera Clase.
Se hace referencia a un tipo de dato que tiene privilegios dentro del lenguaje de programación

hay caracteristicas generales en los diferentes lenguajes  que nos indican si un elemento es de primera clase

* Se puede instanciar 
* Puede usar como argumentos.
* Tiene identidad propia
* Puede ser un valor de retorno

En algunos lenguajes suelen estar en practicamente cualquier lugar en java se pueden encontrar:

```java

private int data = 1;

// valor de retorno

private boolean hasLastName(user: User) {
    return !"".equals(user.lastName);
}

```

## Herramientas

Que se necesita para correr programas en haskell ?

* Editor de texto o un IDE con el plugin de haskell
* Compilador de Haskell

***

el GHC (Glasgow haskell compiler ) tiene una forma de interactuar con nuestras funciones, es una forma fantastica para
probar todas las funciones similar a una terminal interactiva como irb de ruby. Ademas de contar con una interfaz para cargar archivos 
etc.
